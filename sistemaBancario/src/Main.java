import br.com.bancario.DAO.Opera;
import br.com.bancario.DAO.OperaClienteFisico;
import br.com.bancario.DAO.OperaClienteJuridico;
import br.com.bancario.DAO.OperaContaCorrente;
import br.com.bancario.DAO.OperaContaEspecial;
import br.com.bancario.DAO.OperaContaPoupanca;
import br.com.bancario.view.ViewMenuGeral;

public class Main {
	public static void main(String args[]) {
		Opera opera = new Opera();
		opera.carregaArrays();
		new ViewMenuGeral();
	}

}
