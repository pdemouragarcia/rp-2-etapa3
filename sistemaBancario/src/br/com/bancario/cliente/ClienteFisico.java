package br.com.bancario.cliente;

import br.com.bancario.DAO.OperaClienteFisico;

public class ClienteFisico extends Cliente {
	private String Cpf;
	private String Rg;

	public String getCpf() {
		return Cpf;
	}

	public void setCpf(String cpf) {
		Cpf = cpf;
	}

	public String getRg() {
		return Rg;
	}

	public void setRg(String rg) {
		Rg = rg;
	}

	public void addCliente(String nome, String endereco, String cep, String telefone, String renda, String cpf,
			String rg) {
		this.idCliente = Cliente.id++;
	}

}
