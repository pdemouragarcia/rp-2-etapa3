package br.com.bancario.contas;

import java.util.Date;

public class ContaPoupanca extends ContaCorrente {

	public ContaPoupanca(int nmro, boolean situacao, double saldo, Date dataAbertura, int senha,
			String identificacao) {
		super(nmro, situacao, saldo, dataAbertura, senha, identificacao);
		// TODO Auto-generated constructor stub
	}

	private String juros;

	public String getJuros() {
		return juros;
	}

	public void setJuros(String juros) {
		this.juros = juros;
	}

}
