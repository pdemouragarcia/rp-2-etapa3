package br.com.bancario.contas;

import java.io.Serializable;
import java.util.Date;

public class ContaEspecial extends ContaCorrente {
	public ContaEspecial(int nmro, boolean situacao, double saldo, Date dataAbertura, int senha,
			String identificacao) {
		super(nmro, situacao, saldo, dataAbertura, senha, identificacao);
		// TODO Auto-generated constructor stub
	}

	double limite = 0;

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	@Override
	public boolean saca(double valor, int senha) {
		if ((validaSenha(senha) == true) && (valor <= (this.saldo + this.limite))) {
			this.saldo = this.saldo - valor;
			System.out.print("Saque efetuado com sucesso");
			return true;
		} else {
			System.out.print("Saldo indisponível");
			return false;
		}
	}

	public void alteraLimite(double valor, int senha) {
		if (validaSenha(senha) == true) {
			this.setLimite(valor);
		} else {
			System.out.print("Senha incorreta");

		}

	}
}
