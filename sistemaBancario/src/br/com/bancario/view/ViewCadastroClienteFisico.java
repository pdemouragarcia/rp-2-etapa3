package br.com.bancario.view;


/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import br.com.bancario.cliente.ClienteFisico;


@SuppressWarnings("serial")
public class ViewCadastroClienteFisico extends JFrame implements ActionListener {
	private JLabel label, labelCPF, labelRg;
	private JTextField fieldRG, fieldCPF, fieldNome, fieldEndereco, fieldCep, fieldTelefone, fieldRenda;
	private JButton buttonCadastrar, buttonCancelar;
	
 //   private JRadioButton CheckAtiva, CheckInativa;
 //   private ButtonGroup Status;


	public ViewCadastroClienteFisico() {
		super("Cadastrar Novo Cliente");		//titulo do programa
		Container content = getContentPane();
		content.setLayout(null);
		setSize(845, 535);
		setFont(new Font("Arial", Font.BOLD, 12));
		
		/*
		 * �NICIO DOS CAMPOS TELA DE CADASTRO
		 * (x, y, a, b)
		 * x = posi��o horizontal, y = posi��o vertical
		 * a = tamanho horizontal, b = tamanho vertical 
		 */

		String labelDoCpf = "CPF: ";
		labelCPF = new JLabel(labelDoCpf);
		labelCPF.setBounds(new Rectangle(220, 70, 125, 25)); //(x, y, a, b)
		content.add(labelCPF);
		fieldCPF = new JTextField();
		fieldCPF.setBounds(new Rectangle(340, 70, 200, 25));
		content.add(fieldCPF, null);
		
		String labelDoRG = "RG: ";
		labelRg = new JLabel(labelDoRG);
		labelRg.setBounds(new Rectangle(220, 110, 125, 25)); //(x, y, a, b)
		content.add(labelRg);
		fieldRG = new JTextField();
		fieldRG.setBounds(new Rectangle(340, 110, 200, 25));
		content.add(fieldRG, null);
	
		
		String labelDoNome = "Nome Completo: ";
		label = new JLabel(labelDoNome);
		label.setBounds(new Rectangle(220, 150, 125, 25));
		content.add(label);
		fieldNome = new JTextField();
		fieldNome.setBounds(new Rectangle(340, 150, 200, 25));
		content.add(fieldNome, null);
		
		String labelDoEndereco = "Endere�o: ";
		label = new JLabel(labelDoEndereco);
		label.setBounds(new Rectangle(220, 190, 125, 25));
		content.add(label);
		fieldEndereco = new JTextField();
		fieldEndereco.setBounds(new Rectangle(340, 190, 200, 25));
		content.add(fieldEndereco, null);
		
		String labelDoCep = "CEP: ";
		label = new JLabel(labelDoCep);
		label.setBounds(new Rectangle(220, 230, 125, 25));
		content.add(label);
		fieldCep = new JTextField();
		fieldCep.setBounds(new Rectangle(340, 230, 200, 25));
		content.add(fieldCep, null);
		
		String labelDoTelefone = "Telefone: ";
		label = new JLabel(labelDoTelefone);
		label.setBounds(new Rectangle(220, 270, 125, 25));
		content.add(label);
		fieldTelefone = new JTextField();
		fieldTelefone.setBounds(new Rectangle(340, 270, 200, 25));
		content.add(fieldTelefone, null);
		
		String labelDaRenda = "Renda: ";
		label = new JLabel(labelDaRenda);
		label.setBounds(new Rectangle(220, 310, 125, 25));
		content.add(label);
		fieldRenda = new JTextField();
		fieldRenda.setBounds(new Rectangle(340, 310, 200, 25));
		content.add(fieldRenda, null);
		
		
		/*
		String labelDaSituacao = "Situa��o: ";
		label = new JLabel(labelDaSituacao);
		label.setBounds(new Rectangle(220, 350, 125, 25));
		content.add(label);
		CheckAtiva = new JRadioButton("Ativo", false);
		CheckInativa = new JRadioButton("Inativo", false);
		add(CheckAtiva);
		add(CheckInativa);
		Status = new ButtonGroup();
        Status.add(CheckAtiva);
        Status.add(CheckInativa); 
		
		
		CheckAtiva.setBounds(new Rectangle(340, 350, 200, 25));
		CheckInativa.setBounds(new Rectangle(340, 390, 200, 25));
		 
		 */
		

		
		/*
		 * FIM DOS CAMPOS TELA DE CADASTRO
		 */


		/*
		 * �NICIO BOT�ES CADASTRAR E CANCELAR
		 */

		
		buttonCadastrar = new JButton("SALVAR CADASTRO");
		buttonCadastrar.setBounds(new Rectangle(230, 440, 200, 35));
		content.add(buttonCadastrar, null);
		buttonCadastrar.setActionCommand("OK");
		buttonCadastrar.addActionListener(this);
		buttonCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Add.png")));
		

		buttonCancelar = new JButton("CANCELAR");
		buttonCancelar.setBounds(new Rectangle(480, 440, 200, 35));
		content.add(buttonCancelar, null);
		buttonCancelar.setActionCommand("CANCELAR");
		buttonCancelar.addActionListener(this);
		buttonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Delete.png")));

		setVisible(true);
		setResizable(false); //N�o deixa a tela ser redimensionada
		

		/*
		 * FIM BOT�ES CADASTRAR E CANCELAR
		 */
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		if (comando.equals("CANCELAR")) { //fun��o do bot�o cancelar
			new ViewMenuGeral();
			this.dispose();
		}
		
		if (comando.equals("OK")) { //fun��o do bot�o ok
		//	String cpf = fieldCPF.getText();
		//	String cnpj = fieldCNPJ.getText();
			String nome = fieldNome.getText();
			String endereco = fieldEndereco.getText();
			String cep = fieldCep.getText();
			String telefone = fieldTelefone.getText();
			String renda = fieldRenda.getText();
			String cpf = fieldCPF.getText();
			String rg = fieldRG.getText();
			
			
			
			ClienteFisico cliF = new ClienteFisico();
			cliF.addCliente(cpf, rg, nome, endereco, cep, telefone, renda);
			//not working... not yet
	
		}
	}
}
