package br.com.bancario.view;

/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;


@SuppressWarnings("serial")
public class ViewMenuCliente extends JFrame implements ActionListener {
	private JButton buttonCadastrar, buttonConsultar, buttonExit;
	
	public ViewMenuCliente() {
		super("Menu Contas");
		Container content = getContentPane();
		content.setLayout(null);

		Font font = new Font("Serif", Font.PLAIN, 12);
		content.setFont(font);

		// botao cadastrar
		buttonCadastrar = new JButton("Cadastrar Cliente F�sico");
		buttonCadastrar.setBounds(110, 130, 240, 40);
		content.add(buttonCadastrar);
		buttonCadastrar.setActionCommand("FISICO");
		buttonCadastrar.addActionListener(this);

		// botao consultar
		buttonConsultar = new JButton("Cadastrar Cliente Jur�dico");
		buttonConsultar.setBounds(110, 190, 240, 40);
		content.add(buttonConsultar);
		buttonConsultar.setActionCommand("JURIDICO");
		buttonConsultar.addActionListener(this);

		// Botao Sair
		buttonExit = new JButton("Sair");
		buttonExit.setBounds(new Rectangle(110, 365, 240, 40));
		buttonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Exit.png")));
		content.add(buttonExit, null);
		buttonExit.setActionCommand("SAIR");
		buttonExit.addActionListener(this);

		setVisible(true);
		setSize(420, 500);
		setResizable(false);

	}

	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
			if (comando.equals("FISICO")) {
				new ViewCadastroClienteFisico();
				this.dispose();
			}
			if (comando.equals("JURIDICO")) {
				new ViewCadastroClienteJuridico();
				this.dispose();
				
			}
			if (comando.equals("SAIR")) {
				new ViewMenuConta();
				this.dispose();
			}
		}
}
