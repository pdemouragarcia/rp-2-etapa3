package br.com.bancario.view;

/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class ViewMenuGeral extends JFrame implements ActionListener {
	private JButton buttonCadastrar, buttonConsultar, buttonRelatorioGeral, buttonExit;
	private JLabel labelTitulo;

	public ViewMenuGeral() {
		super("Menu Principal");
		Container content = getContentPane();
		content.setLayout(null);

		Font font = new Font("Serif", Font.PLAIN, 12);
		content.setFont(font);

		String labelText = "";
		labelTitulo = new JLabel(labelText);
		labelTitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bemVindo.png")));
		labelTitulo.setBounds(new Rectangle(85, 40, 425, 95));
		content.add(labelTitulo);

		// botao cadastrar
		buttonCadastrar = new JButton("Cadastrar Novo Cliente");
		buttonCadastrar.setBounds(110, 130, 200, 40);
		buttonCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Person.png")));
		content.add(buttonCadastrar);
		buttonCadastrar.setActionCommand("CADASTRARCLIENTE");
		buttonCadastrar.addActionListener(this);

		// botao consultar
		buttonConsultar = new JButton("Cadastrar Nova Conta");
		buttonConsultar.setBounds(110, 190, 200, 40);
		buttonConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Add.png")));
		content.add(buttonConsultar);
		buttonConsultar.setActionCommand("CADASTRARCONTA");
		buttonConsultar.addActionListener(this);

		// botao relatorio geral
		buttonRelatorioGeral = new JButton("Relatorio Geral");
		buttonRelatorioGeral.setBounds(new Rectangle(110, 250, 200, 40));
		buttonRelatorioGeral.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/View.png")));
		content.add(buttonRelatorioGeral, null);
		buttonRelatorioGeral.setActionCommand("RELATORIO");
		buttonRelatorioGeral.addActionListener(this);

		// Botao Sair
		buttonExit = new JButton("Sair");
		buttonExit.setBounds(new Rectangle(110, 365, 200, 40));
		buttonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Exit.png")));
		content.add(buttonExit, null);
		buttonExit.setActionCommand("EXIT");
		buttonExit.addActionListener(this);

		setVisible(true);
		setSize(420, 500);
		setResizable(false);

	}

	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
			if (comando.equals("CADASTRARCLIENTE")) {
				new ViewMenuCliente();
				this.dispose();
			}
			if (comando.equals("CADASTRARCONTA")) {
				new ViewMenuConta();
				this.dispose();
			}/*
			if (comando.equals("CONSULTAR")) {
				new JanelaConsulta();
				this.dispose();
			}*/
			if (comando.equals("EXIT")) {
				System.exit(0);
			}
		}
}
