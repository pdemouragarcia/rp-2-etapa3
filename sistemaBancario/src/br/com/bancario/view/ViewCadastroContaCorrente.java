package br.com.bancario.view;


/*
 * author @ggirardon
 */

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
//import javax.swing.JLabel;


@SuppressWarnings("serial")
public class ViewCadastroContaCorrente extends JFrame implements ActionListener {
//	private JLabel label;
	//private JTextField fieldNumeroConta, fieldDataAbertura, fieldDataEncerramento, fieldSituacao, fieldSenha, fieldSaldo;
	private JButton buttonCadastrar, buttonCancelar;
//	private JRadioButton tipoContaCorrente, tipoContaCorrentePoupanca, tipoContaCorrenteEspecial;



	public ViewCadastroContaCorrente() {
		super("Cadastrar Nova Conta");		//titulo do programa
		Container content = getContentPane();
		content.setLayout(null);
		setSize(845, 535);
		setFont(new Font("Arial", Font.BOLD, 12));
		
		/*
		 * �NICIO DOS CAMPOS TELA DE CADASTRO
		 * (x, y, a, b)
		 * x = posi��o horizontal, y = posi��o vertical
		 * a = tamanho horizontal, b = tamanho vertical 
		 */
		
		
		//aaaa
		
		/*
		 * FIM DOS CAMPOS TELA DE CADASTRO
		 */


		/*
		 * �NICIO BOT�ES CADASTRAR E CANCELAR
		 */

		
		buttonCadastrar = new JButton("SALVAR CADASTRO");
		buttonCadastrar.setBounds(new Rectangle(230, 440, 200, 35));
		content.add(buttonCadastrar, null);
		buttonCadastrar.setActionCommand("OK");
		buttonCadastrar.addActionListener(this);
		buttonCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Add.png")));
		

		buttonCancelar = new JButton("CANCELAR");
		buttonCancelar.setBounds(new Rectangle(480, 440, 200, 35));
		content.add(buttonCancelar, null);
		buttonCancelar.setActionCommand("CANCELAR");
		buttonCancelar.addActionListener(this);
		buttonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Delete.png")));

		setVisible(true);
		setResizable(false); //N�o deixa a tela ser redimensionada
		

		/*
		 * FIM BOT�ES CADASTRAR E CANCELAR
		 */
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		if (comando.equals("CANCELAR")) { //fun��o do bot�o cancelar
			new ViewMenuConta();
			this.dispose();
		}
		
		
	}
}
