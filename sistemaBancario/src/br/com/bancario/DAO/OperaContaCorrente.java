package br.com.bancario.DAO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

import br.com.bancario.contas.ContaCorrente;

public class OperaContaCorrente {
	private List<ContaCorrente> contasCorrentes = new ArrayList<ContaCorrente>();

	public List<ContaCorrente> getContasCorrentes() {
		return contasCorrentes;
	}

	public void addContaCorrente(ContaCorrente cc) {
		contasCorrentes.add(cc);
		escreveVetor();
	}

	public void atualizaContaCorrente(int nmro, ContaCorrente cc) {
		for (int i = 0; i < contasCorrentes.size(); i++) {
			if (contasCorrentes.get(i).getNumeroConta() == nmro) {
				contasCorrentes.set(i, cc);
				escreveVetor();
			}
		}
	}

	public void removeContaCorrente(ContaCorrente cc) {
		contasCorrentes.remove(cc);
		escreveVetor();
	}

	public void escreveVetor() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/contasCorrentes.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasCorrentes);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void recuperaVetor() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/contasCorrentes.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasCorrentes = (ArrayList<ContaCorrente>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaCorrente(String id, double valor, int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getIdentificacao() == id) && (conta.saca(valor, senha) == true)) {
				atualizaContaCorrente(nmro, conta);
			}
		}
	}

	public void depositaDaContaCorrente(double valor, int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getNumeroConta() == nmro) && (conta.deposita(valor, senha) == true)) {
				System.out.println(" " + conta.getSaldo());
				atualizaContaCorrente(nmro, conta);
				System.out.println("depositou ");
			} else {
				System.out.println("Nao depositou ");
			}
		}
	}

	public void encerraConta(String id, int senha, int nmro) {
		for (ContaCorrente conta : contasCorrentes) {
			if ((conta.getIdentificacao() == id) && (conta.encerraConta(senha) == true)) {
				atualizaContaCorrente(nmro, conta);
			}
		}
	}

	public void criaContaCorrente(int nmro, boolean situacao, double saldo, Date dataAbertura, int senha,
			String identificacao) {
		ContaCorrente cc = new ContaCorrente(nmro, situacao, saldo, dataAbertura, senha, identificacao);
		addContaCorrente(cc);
	}
}