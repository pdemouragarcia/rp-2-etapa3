package br.com.bancario.DAO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.bancario.cliente.ClienteFisico;
import br.com.bancario.cliente.ClienteJuridico;;

public class OperaClienteFisico {
	private List<ClienteFisico> clientesFisicos = new ArrayList<ClienteFisico>();

	public List<ClienteFisico> getClientesFisicos() {
		return clientesFisicos;
	}

	public void addClienteFisico(ClienteFisico p) {
		clientesFisicos.add(p);
		JOptionPane.showMessageDialog(null, " Cadastro de Cliente F�sico Realizado ", "",
				JOptionPane.INFORMATION_MESSAGE);
		escreveVetor();
	}

	public void atualizaClienteFisico(int ID, ClienteFisico p) {
		for (int i = 0; i < clientesFisicos.size(); i++) {
			if (clientesFisicos.get(i).getId() == ID) {
				clientesFisicos.set(i, p);
				escreveVetor();
			}
		}
	}

	public void removeClienteFisico(ClienteFisico p) {
		clientesFisicos.remove(p);
		escreveVetor();
	}

	public void escreveVetor() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/clientesFisicos.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(clientesFisicos);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetor() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/clientesFisicos.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			clientesFisicos = (ArrayList<ClienteFisico>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}