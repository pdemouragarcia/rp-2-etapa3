package br.com.bancario.DAO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.bancario.cliente.ClienteJuridico;
import br.com.bancario.contas.ContaCorrente;

public class OperaClienteJuridico {
	private List<ClienteJuridico> clientesJuridicos = new ArrayList<ClienteJuridico>();

	public List<ClienteJuridico> getClientesJuridicos() {
		return clientesJuridicos;
	}

	public void addClienteJuridico(ClienteJuridico p) {
		clientesJuridicos.add(p);
		escreveVetor();
	}

	public void atualizaClienteJuridico(int ID, ClienteJuridico p) {
		for (int i = 0; i < clientesJuridicos.size(); i++) {
			if (clientesJuridicos.get(i).getId() == ID) {
				clientesJuridicos.set(i, p);
				escreveVetor();
			}
		}
	}

	public void removeClienteJuridico(ClienteJuridico p) {
		clientesJuridicos.remove(p);
		escreveVetor();
	}

	public void escreveVetor() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/clientesJuridicos.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(clientesJuridicos);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetor() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/clientesJuridicos.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			clientesJuridicos = (ArrayList<ClienteJuridico>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}