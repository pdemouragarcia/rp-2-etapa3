package br.com.bancario.DAO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.bancario.contas.ContaCorrente;
import br.com.bancario.contas.ContaEspecial;

public class OperaContaEspecial {
	private List<ContaEspecial> contasEspeciais = new ArrayList<ContaEspecial>();

	public List<ContaEspecial> getContasEspeciais() {
		return contasEspeciais;
	}

	public void addContaEspecial(ContaEspecial ce) {
		contasEspeciais.add(ce);
		escreveVetor();
	}

	public void atualizaContaEspecial(int nmro, ContaEspecial ce) {
		for (int i = 0; i < contasEspeciais.size(); i++) {
			if (contasEspeciais.get(i).getNumeroConta() == nmro) {
				contasEspeciais.set(i, ce);
				escreveVetor();
			}
		}
	}

	public void removeContaEspecial(ContaEspecial cce) {
		contasEspeciais.remove(cce);
		escreveVetor();
	}

	public void escreveVetor() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/contasEspeciais.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasEspeciais);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetor() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/contasEspeciais.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasEspeciais = (ArrayList<ContaEspecial>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaEspecial(String id, double valor, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getIdentificacao() == id) && (conta.saca(valor, senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}
	}

	public void depositaDaContaEspecial(String id, double valor, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getIdentificacao() == id) && (conta.deposita(valor, senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}

	}

	public void encerraContaEspecial(String id, int senha, int nmro) {
		for (ContaEspecial conta : contasEspeciais) {
			if ((conta.getIdentificacao() == id) && (conta.encerraConta(senha) == true)) {
				atualizaContaEspecial(nmro, conta);
			}
		}
	}
}
