package br.com.bancario.DAO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.bancario.contas.ContaCorrente;
import br.com.bancario.contas.ContaPoupanca;;

public class OperaContaPoupanca {
	private List<ContaPoupanca> contasPoupanca = new ArrayList<ContaPoupanca>();

	public List<ContaPoupanca> getContasPoupanca() {
		return contasPoupanca;
	}

	public void addConta(ContaPoupanca p) {
		contasPoupanca.add(p);
		escreveVetor();
	}

	public void atualizaConta(int nmro, ContaPoupanca cp) {
		for (int i = 0; i < contasPoupanca.size(); i++) {
			if (contasPoupanca.get(i).getNumeroConta() == nmro) {
				contasPoupanca.set(i, cp);
				escreveVetor();
			}
		}
	}

	public void removeConta(ContaPoupanca p) {
		contasPoupanca.remove(p);
		escreveVetor();
	}

	public void escreveVetor() {
		try {
			// Gera o arquivo para armazenar o objeto
			FileOutputStream arquivoGrav = new FileOutputStream("C:/arquivosRP/contasPoupanca.dat");
			// Classe responsavel por inserir os objetos
			ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
			// Grava o objeto cliente no arquivo
			// writeObject(objGravar);
			objGravar.writeObject(contasPoupanca);
			objGravar.flush();
			objGravar.close();
			arquivoGrav.flush();
			arquivoGrav.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recuperaVetor() {
		try {
			// Carrega o arquivo
			FileInputStream arquivoLeitura = new FileInputStream("C:/arquivosRP/contasPoupanca.dat");
			// Classe responsavel por recuperar os objetos do arquivo
			ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
			contasPoupanca = (ArrayList<ContaPoupanca>) objLeitura.readObject();
			// readObject(objLeitura);
			objLeitura.close();
			arquivoLeitura.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sacaDaContaPoupanca() {
		// TODO Auto-generated method stub
		
	}

	public void depositaDaContaPoupanca() {
		// TODO Auto-generated method stub
		
	}

	public void encerraContaPoupanca() {
		// TODO Auto-generated method stub
		
	}
}
